#!/usr/bin/env bash

function line_update {
    # 1 search regex, 2 replacement, 3 file
    if grep -xq "^$1.*$" $3
    then
        # present
        sed -i "s/^$1.*$/$2/g" $3
    else
        # not present
        echo >> $3
        echo $2 >> $3
    fi
}

function append_nginx {
    COMM='/#PLACEHOLDER#/i '"\    $@"
    sed -i "$COMM" /etc/nginx/sites-available/default
}

SERVER_NAME=${SERVER_NAME:-_}
TRUSTED_NETWORK=${TRUSTED_NETWORK:-''}

if [ ! -f /bootstrapped ]
then

# Server name
append_nginx "server_name $SERVER_NAME;"

# Trusted proxies

# Configure network to allow only trusted proxyes
#append_nginx "allow $TRUSTED_NETWORK;"
#append_nginx "deny all;"

if [ ! -z $TRUSTED_NETWORK ];
then
    echo "Trusted network config detected";
    append_nginx "set_real_ip_from $TRUSTED_NETWORK;"
    append_nginx "real_ip_header X-Forwarded-For;"
    append_nginx "real_ip_recursive on;"
fi

# SSL

if [ -f /certs/public.crt ] && [ -f /certs/private.key ]
then
    echo "SSL certificates detected"

    cat <<EOF > /etc/nginx/sites-available/onlyhttps
server {
    listen 80;

    location / {
        return 301 https://\$host\$request_uri;
    }
}
EOF

    append_nginx "ssl_certificate /cert/public.crt;"
    append_nginx "ssl_certificate_key /cert/private.key;"
    append_nginx "ssl on;"
    append_nginx "ssl_session_cache  builtin:1000  shared:SSL:10m;"
    append_nginx "ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;"
    append_nginx "ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;"
    append_nginx "ssl_prefer_server_ciphers on;"

    line_update " *listen" "listen 443;" /etc/nginx/sites-available/default
fi

  touch /bootstrapped
fi


cd /webroot

if [ "$1" != "" ] && [ "$1" != "force-serve" ];
then
    PRETEND=$(php artisan migrate --pretend --force)

    if [ -f /webroot/storage/logs/laravel.log ]
    then
      cat /webroot/storage/logs/laravel.log
      rm /webroot/storage/logs/laravel.log
    fi


    if [ "$PRETEND" != "Nothing to migrate." ]; then
        echo "There are pending migrations, please execute them"
        exit 1
    fi
fi

case $1 in
    "")
       usermod --shell /bin/bash www-data
       bash
    ;;
    "serve")
       chown -R www-data:www-data /webroot/storage/logs
       supervisord -c /supervisor.conf
    ;;

    "force-serve")
       chown -R www-data:www-data /webroot
       supervisord -c /supervisor.conf
    ;;

    "queue")
       usermod --shell /bin/bash www-data
       COMMAND="php artisan queue:work --tries=3"
       echo "---- RUNNING $COMMAND"
       su www-data -c "$COMMAND"
    ;;

    "cron")
        usermod --shell /bin/bash www-data
        while [ 1 ]
        do
            DATE=`date '+%Y-%m-%d %H:%M:%S'`
            echo "### $DATE (cron run)"
            echo "### $DATE" >> /webroot/storage/logs/cron.log
            COMMAND="php artisan schedule:run"
            echo "---- RUNNING $COMMAND"
            RES=`su www-data -c "$COMMAND"`
            echo "$RES"
            echo "$RES" >> /webroot/storage/logs/cron.log

            sleep 60
        done
    ;;
    *)
       usermod --shell /bin/bash www-data
       COMMAND="php artisan $@"
       echo "---- RUNNING $COMMAND"
       su www-data -c "$COMMAND"
    ;;
esac